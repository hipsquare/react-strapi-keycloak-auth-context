# Changelog

## 0.0.13

Only load the user profile once successfully logged in. So far, the library tried to load the user profile always after checking the authentication status, and even when the user was not authenticated would fetch the profile. This would lead to error messages in the console.

# @hipsquare/react-strapi-keycloak-auth-context

This library provides a React context to manage authenticaton in conjunction with the Strapi plugin to authenticate with Keycloak: [`@hipsqurae/strapi-plugin-keycloak`](https://gitlab.com/hipsquare/strapi-plugin-keycloak).

It handles logging in and logging out, provides information on the current login state and supplies components to display content only to users that are logged in or logged out.

## Installation

```shell
yarn add @hipsquare/react-strapi-keycloak-auth-context
```

## Usage

Wrap your application into a `WithAuthContext` element:

```tsx
import { WithAuthContext } from '@hipsquare/react-strapi-keycloak-auth-context';

const STRAPI_BASE_URL = 'http://localhost:1337';

export const App = () => {
  return (
    <WithAuthContext strapiBaseUrl={STRAPI_BASE_URL}>
      {/* the rest of your app */}
    </WithAuthContext>
  );
};
```

You can also set a `redirectToUrlAfterLogin` property. The user will be redirected back there after the login. This can be useful to forward the user to a deep-link:

```tsx
import { WithAuthContext } from '@hipsquare/react-strapi-keycloak-auth-context';

const STRAPI_BASE_URL = 'http://localhost:1337';

export const App = () => {
  return (
    <WithAuthContext
      strapiBaseUrl={STRAPI_BASE_URL}
      redirectToUrlAfterLogin="https://my-frontend.com/my-deep-link"
    >
      {/* the rest of your app */}
    </WithAuthContext>
  );
};
```

### Checking Login Status

To get information on the authentication status or initiate login and logout, you can use the `useAuth` hook:

```typescript
import { useAuth } from '@hipsquare/react-strapi-keycloak-auth-context';

const {
  isAuthenticated,
  login,
  logout,
  fetchAuthenticationStatus,
  authenticationInProcess,
} = useAuth();
```

- `isAuthenticated` contains `true` if a user is logged in and `false` if no user is logged in.
- `login` is a function and can be called to initiate a new login process.
- `logout` is a function and can be called to logout the user.
- `fetchAuthenticationStatus` is a function and can be called to forcefully fetch the login status.
- `authenticationInProcess` contains `true` if the plugin is currently unsure whether or not a user is logged in and is in the process of finding out.

### Displaying components based on authentication status

To make it easy to display or hide components based on the authentication status, you can use the following components:

- `OnlyDisplayIfLoggedIn`: Display children only if a user is currently logged in.
- `OnlyDisplayIfLoggingIn` Display children only if the plugin is in the process of figuring out whether or not a user is logged in. Practical to display loading spinners.
- `OnlyDisplayIfLoggedOut`: Display children only if no user is logged in.

For example:

```tsx
import {
  OnlyDisplayIfLoggedIn,
  useAuth,
} from '@hipsquare/react-strapi-keycloak-auth-context';

export const MyComponent = () => {
  const { logout } = useAuth();

  return (
    <>
      <OnlyDisplayIfLoggedIn>
        I am only rendered if a user is logged in.
        <button onClick={logout}>Logout</button>
      </OnlyDisplayIfLoggedIn>
    </>
  );
};
```

### Fetching from Strapi routes protected by the Keycloak middleware

When you fetch data from Strapi routes that are protected by the [Keycloak middleware](https://gitlab.com/hipsquare/strapi-plugin-keycloak/-/blob/main/README.md#protecting-strapi-routes), you can use the convenient `fetch` function returned by `useAuth`. The function ensures that `credentials: true` is set for these requests and therefore HTTP only cookies work. You can use it as a drop-in replacement for `fetch`:

```typescript
const { fetch } = useAuth();
fetch(`http://localhost:1337/api/protected-route`)
  .then(res => res.json())
  .then(data => console.log('Data from protected route:', data));
```

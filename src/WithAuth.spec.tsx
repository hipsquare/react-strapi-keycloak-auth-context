import {
  act,
  fireEvent,
  render,
  renderHook,
  RenderHookResult,
  waitFor,
} from '@testing-library/react';
import {
  IAuthContext,
  IUserProfile,
  OnlyDisplayIfLoggedIn,
  OnlyDisplayIfLoggedOut,
  useAuth,
  WithAuthContext,
} from './WithAuth';

function TestComponentPerformingLogin(): JSX.Element {
  const { login } = useAuth();
  return <button onClick={login}>Login</button>;
}

function TestComponentPerformingLogout(): JSX.Element {
  const { logout } = useAuth();
  return <button onClick={() => logout()}>Logout</button>;
}

function TestComponentShowingAuthenticationState(): JSX.Element {
  const { isAuthenticated, fetchAuthenticationStatus } = useAuth();
  return (
    <>
      <p>{`${isAuthenticated}`}</p>
      <button onClick={() => fetchAuthenticationStatus()}>Detect State</button>
    </>
  );
}

describe('WithAuth', () => {
  const strapiBaseUrl = 'https://my-strapi.com';
  let windowLocationMock: Location;
  // stores the original `window.location` object to restore it after
  let windowLocationOriginal: Location;

  beforeEach(() => {
    windowLocationOriginal = window.location;

    windowLocationMock = {
      href: 'test-href',
    } as unknown as Location;

    Object.defineProperty(window, 'location', {
      writable: true,
      value: windowLocationMock,
    });

    global.fetch = vi
      .fn()
      .mockResolvedValue({ json: vi.fn().mockResolvedValue({}) });
  });

  it('should render the hook', () => {
    // setup & test
    let container;
    const result = render(
      <WithAuthContext strapiBaseUrl={strapiBaseUrl}>
        <TestComponentPerformingLogin />
      </WithAuthContext>,
    );
    container = result;

    // assert
    expect(container).toBeTruthy();
  });

  it('should load the login status when initially starting', async () => {
    const fetchSpy = vi.spyOn(global, 'fetch').mockResolvedValue({
      json: vi.fn().mockResolvedValue(false),
    } as unknown as Response);

    // setup & test
    await act(() => {
      render(
        <WithAuthContext strapiBaseUrl={strapiBaseUrl}>
          <TestComponentPerformingLogin />
        </WithAuthContext>,
      );
    });

    // assert
    await waitFor(() => expect(fetchSpy).toHaveBeenCalled());
    fetchSpy.mockRestore();
  });

  it('should redirect to the IdP when calling #login and attach the current location HREF as the redirect URL', () => {
    // setup & test
    const { container } = render(
      <WithAuthContext strapiBaseUrl={strapiBaseUrl}>
        <TestComponentPerformingLogin />
      </WithAuthContext>,
    );

    act(() => {
      fireEvent.click(container.querySelector('button')!);
    });

    // assert
    expect(windowLocationMock.href).toEqual(
      `${strapiBaseUrl}/keycloak/login?redirectTo=test-href`,
    );
  });

  it('should set the login state to authenticated if the isLoggedIn API route returns true', async () => {
    vi.useFakeTimers();
    // setup
    const fetchSpy = vi.spyOn(global, 'fetch').mockResolvedValue({
      json: vi.fn().mockResolvedValue(true),
    } as never);

    // test

    const result = render(
      <WithAuthContext strapiBaseUrl={strapiBaseUrl}>
        <>
          <TestComponentPerformingLogin />
          <TestComponentShowingAuthenticationState />
        </>
      </WithAuthContext>,
    );

    act(() => {
      fireEvent.click(result.getByText('Login'));
    });

    act(() => {
      fireEvent.click(result.getByText('Detect State'));
    });

    waitFor(() => {
      expect(result.getByText('true')).toBeInTheDocument();
    });
    // assert

    // clean up
    fetchSpy.mockClear();
  });

  it('should set the login state to unauthenticated if the isLoggedIn API route returns false', async () => {
    // setup
    const fetchSpy = vi.spyOn(global, 'fetch').mockResolvedValue({
      json: vi.fn().mockResolvedValue(false),
    } as never);

    // test
    const { findByText, getByText } = render(
      <WithAuthContext strapiBaseUrl={strapiBaseUrl}>
        <>
          <TestComponentPerformingLogin />
          <TestComponentShowingAuthenticationState />
        </>
      </WithAuthContext>,
    );

    act(() => {
      fireEvent.click(getByText('Login'));
    });

    act(() => {
      fireEvent.click(getByText('Detect State'));
    });

    // assert
    expect(await findByText('false')).toBeInTheDocument();

    // clean up
    fetchSpy.mockClear();
  });

  it('should log out the user with cookies', async () => {
    // setup
    const fetchSpy = vi.spyOn(global, 'fetch').mockResolvedValue({
      json: vi.fn().mockResolvedValue({}),
    } as never);

    // test
    const { findByText, getByText } = render(
      <WithAuthContext strapiBaseUrl={strapiBaseUrl}>
        <>
          <TestComponentShowingAuthenticationState />
          <TestComponentPerformingLogout />
        </>
      </WithAuthContext>,
    );

    act(() => {
      fireEvent.click(getByText('Logout'));
    });

    // assert
    expect(fetchSpy).toHaveBeenCalled();

    expect(await findByText('false')).toBeInTheDocument();

    // clean up
    fetchSpy.mockClear();
  });

  describe('OnlyDisplayIfLoggedIn', () => {
    it('should not display children of OnlyDisplayIfLoggedIn if the user is not authenticated and instead show the login button', async () => {
      // setup -- set authenticated to false by mocking the fetch response
      vi.spyOn(global, 'fetch').mockResolvedValue({
        json: vi.fn().mockResolvedValue(false),
      } as never);

      // test
      const { queryByText, findByText, getByText } = render(
        <WithAuthContext strapiBaseUrl={strapiBaseUrl}>
          <>
            <TestComponentShowingAuthenticationState />
            <OnlyDisplayIfLoggedIn>
              <p>Show me</p>
            </OnlyDisplayIfLoggedIn>
            <OnlyDisplayIfLoggedOut>
              <button>Login</button>
            </OnlyDisplayIfLoggedOut>
          </>
        </WithAuthContext>,
      );

      act(() => {
        fireEvent.click(getByText('Detect State'));
      });

      // assert
      await waitFor(async () => {
        expect(await findByText('Login')).not.toBeNull();
        expect(await queryByText('Show me')).toBeNull();
      });
    });

    it('should display the children of OnlyDisplayIfLoggedIn if the user is authenticated', async () => {
      // setup -- set authenticated to true by mocking the fetch response
      vi.spyOn(global, 'fetch').mockResolvedValue({
        json: vi.fn().mockResolvedValue(true),
      } as never);

      // test
      const { findByText, getByText } = render(
        <WithAuthContext strapiBaseUrl={strapiBaseUrl}>
          <>
            <TestComponentShowingAuthenticationState />
            <OnlyDisplayIfLoggedIn>
              <p>Show me</p>
            </OnlyDisplayIfLoggedIn>
          </>
        </WithAuthContext>,
      );

      act(() => {
        fireEvent.click(getByText('Detect State'));
      });

      // assert
      expect(await findByText('Show me')).toBeInTheDocument();
    });
  });

  it('should fetch the user profile when checking the login status', async () => {
    // setup
    const userProfile: IUserProfile = {
      name: 'Max Mustermann',
      family_name: 'Mustermann',
      given_name: 'Max',
      preferred_username: 'max.mustermann',
    };

    let result: RenderHookResult<IAuthContext, unknown>;
    const isLoggedInFetchResponseMock = vi.fn().mockResolvedValue(true);
    const profileFetchResponseMock = vi.fn().mockResolvedValue(userProfile);

    vi.spyOn(global, 'fetch').mockImplementation(((url: string) => {
      let result;
      if (url.includes('isLoggedIn')) {
        result = isLoggedInFetchResponseMock;
      } else if (url.includes('profile')) {
        result = profileFetchResponseMock;
      }
      return Promise.resolve({ json: result ?? null });
    }) as unknown as typeof global['fetch']);

    await act(async () => {
      // test
      result = renderHook(() => useAuth(), {
        wrapper: ({ children }) => (
          <WithAuthContext strapiBaseUrl="http://localhost:3000">
            {children}
          </WithAuthContext>
        ),
      });
    });

    // assert
    expect(result!.result.current.userProfile).toBeTruthy();
    expect(result!.result.current.userProfile?.name).toEqual(userProfile.name);
  });

  afterEach(() => {
    window.location = windowLocationOriginal;
    vi.restoreAllMocks();
  });
});

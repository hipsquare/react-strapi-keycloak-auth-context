import {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useState,
} from 'react';

export interface IUserProfile {
  name?: string;
  given_name?: string;
  family_name?: string;
  preferred_username?: string;
}

export interface IAuthContext {
  isAuthenticated: boolean;
  login: () => void;
  logout: () => void;

  fetchAuthenticationStatus: () => void;
  authenticationInProcess: boolean;
  userProfile: IUserProfile | null;
  /**
   * Method to perform an authenticated fetch
   */
  fetch: typeof fetch;
  refresh: () => void;
}

export const AuthContext = createContext<IAuthContext>({
  isAuthenticated: false,
  login: () => {
    // empty on purpose
  },
  logout: () => {
    // empty on purpose
  },
  fetchAuthenticationStatus: () => {},
  authenticationInProcess: false,
  userProfile: null,
  fetch: (() => {}) as any,
  refresh: () => {},
});

export function useAuth(): IAuthContext {
  return useContext(AuthContext);
}

export function WithAuthContext({
  children,
  strapiBaseUrl,
  redirectToUrlAfterLogin,
}: {
  children: JSX.Element | JSX.Element[];
  strapiBaseUrl: string;
  redirectToUrlAfterLogin?: string;
}): JSX.Element {
  const [isAuthenticated, setAuthenticated] = useState<boolean>(false);
  const [authenticationInProcess, setAuthenticationInProcess] =
    useState<boolean>(true);
  const [userProfile, setUserProfile] = useState<IUserProfile | null>(null);

  const authenticatedFetch = useCallback((url: string, options?: any) => {
    return fetch(url, {
      ...(options ?? {}),
      credentials: 'include',
    });
  }, []);

  const fetchUserProfile = () => {
    authenticatedFetch(`${strapiBaseUrl}/keycloak/profile`)
      .then(response => response.json())
      .then(userProfile => {
        setUserProfile(userProfile);
      });
  };

  useEffect(() => {
    fetchAuthenticationStatus();
  }, []);

  useEffect(() => {
    if (isAuthenticated) {
      fetchUserProfile();
    }
  }, [isAuthenticated]);

  const fetchAuthenticationStatus = useCallback(() => {
    setAuthenticationInProcess(true);
    authenticatedFetch(`${strapiBaseUrl}/keycloak/isLoggedIn/`)
      .then(response => response.json())
      .then(isLoggedIn => {
        setAuthenticated(isLoggedIn);
        setAuthenticationInProcess(false);
      })
      .catch(() => {
        setAuthenticated(false);
        setAuthenticationInProcess(false);
      });
  }, []);

  const login = () => {
    window.location.href = `${strapiBaseUrl}/keycloak/login?redirectTo=${
      redirectToUrlAfterLogin || window.location.href
    }`;
  };

  const refresh = useCallback(async () => {
    await authenticatedFetch(`${strapiBaseUrl}/keycloak/refresh`);
  }, [authenticatedFetch]);

  const logout = () => {
    authenticatedFetch(`${strapiBaseUrl}/keycloak/logout`)
      .then(() => {
        setAuthenticated(false);
      })
      .catch(() => {
        setAuthenticated(false);
      });
  };

  return (
    <AuthContext.Provider
      value={{
        isAuthenticated,
        login,
        logout,
        fetchAuthenticationStatus,
        authenticationInProcess,
        userProfile,
        fetch: authenticatedFetch as typeof fetch,
        refresh,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
}

export function OnlyDisplayIfLoggedIn({
  children,
}: {
  children: JSX.Element | JSX.Element[];
}): JSX.Element {
  const { isAuthenticated } = useContext(AuthContext);

  if (isAuthenticated) {
    return <>{children}</>;
  } else {
    return <></>;
  }
}

export function OnlyDisplayIfLoggingIn({
  children,
}: {
  children: JSX.Element | JSX.Element[];
}): JSX.Element {
  const { isAuthenticated, authenticationInProcess } = useContext(AuthContext);

  if (!isAuthenticated && authenticationInProcess) {
    return <>{children}</>;
  } else {
    return <></>;
  }
}

export function OnlyDisplayIfLoggedOut({
  children,
}: {
  children: JSX.Element | JSX.Element[];
}): JSX.Element {
  const { isAuthenticated, authenticationInProcess } = useContext(AuthContext);

  if (!isAuthenticated && !authenticationInProcess) {
    return <>{children}</>;
  } else {
    return <></>;
  }
}
